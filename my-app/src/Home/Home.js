import React from 'react'
import {MoviesProvider} from '../MoviesData/MoviesContext'
import MovieList from '../MoviesData/MovieList'

const Movies = ()=>{
    return(
        <div style={{fontFamily:'Times new roman'}}>
            <MoviesProvider>
                <MovieList/>
            </MoviesProvider>
        </div>
    )
}

export default Movies