import React from 'react'
import {MoviesProvider} from '../MoviesData/MoviesContext'
import MovieForm from '../MoviesData/MovieForm'

const MovieListEditor = ()=>{
    return(
        <>
            <MoviesProvider>
                <MovieForm/>
            </MoviesProvider>
        </>
    )
}

export default MovieListEditor