import React, {useState,useContext} from 'react'
import {MoviesContext} from './MoviesContext'
import Axios from 'axios'

const MovieForm = ()=>{
    const [movie,setMovie]= useContext(MoviesContext)
    const [input,setInput] = useState({
        id:null,
        title:'',
        description:'',
        year: 2020,
        duration: 120,
        genre:'',
        rating: 0,
        image_url:''
    })

    const submitForm = (event)=>{
        event.preventDefault()
        if(input.id===null){
            Axios.post('http://backendexample.sanbercloud.com/api/movies',{title:input.title,
            description:input.description,year:input.year,duration:input.duration,genre:input.genre,
            rating:input.rating,image_url:input.image_url})
            .then(res=>{
                var data=res.data

                setMovie([...movie,{id:data.id,title:data.title,description:data.description,
                year:data.year,duration:data.duration,genre:data.genre,rating:data.rating,
                image_url:data.image_url}])

                setInput({id:null,title:"",description:"",
                year:2020,duration:120,genre:"",rating:0,
                image_url:""})
            })
        } else{
            Axios.put(`http://backendexample.sanbercloud.com/api/movies/${input.id}`,
            {title:input.title,description:input.description,year:input.year,duration:input.duration,genre:input.genre,
            rating:input.rating,image_url:input.image_url})
            .then(res=>{
                var newMovie=movie.map(x=>{
                    if(x.id===input.id){
                        x.title=input.title
                        x.description=input.description
                        x.year=input.year
                        x.duration=input.duration
                        x.genre=input.genre
                        x.rating=input.rating
                        x.image_url=input.image_url
                    }
                    return x
                })
                setMovie(newMovie)

                setInput({id:null,title:"",description:"",
                year:2020,duration:120,genre:"",rating:0,
                image_url:""})
            })
        }
    }

    const changeFormTitle = (event)=>{
        var value=event.target.value
        setInput({...input,title:value})
    }
    const changeFormDescription=(event)=>{
        var value=event.target.value
        setInput({...input,description:value})
    }
    const changeFormYear=(event)=>{
        var value=event.target.value
        setInput({...input,year:value})
    }
    const changeFormDuration=(event)=>{
        var value=event.target.value
        setInput({...input,duration:value})
    }
    const changeFormGenre=(event)=>{
        var value=event.target.value
        setInput({...input,genre:value})
    }
    const changeFormRating=(event)=>{
        var value=event.target.value
        setInput({...input,rating:value})
    }
    const changeFormImageURL=(event)=>{
        var value=event.target.value
        setInput({...input,image_url:value})
    }

    const deleteForm=(event)=>{
        var idMovie=event.target.value
        Axios.delete( `http://backendexample.sanbercloud.com/api/movies/${idMovie}`)
        .then(res=>{
            var newMovie=movie.filter(x=>x.id!==idMovie)
            setMovie(newMovie)
        })
    }


    return(
        <>
            <div className={'content-mle'}>
                <input type='text' className={'input-search'}/>
                <button className={'input-search'}>Search</button>
                <h1>Daftar Film</h1>
                <table className={'film-list'}>
                    <thead>
                        <tr>
                            <td><b>No</b></td>
                            <td><b>Title</b></td>
                            <td><b>Description</b></td>
                            <td><b>Year</b></td>
                            <td><b>Duration</b></td>
                            <td><b>Genre</b></td>
                            <td><b>Rating</b></td>
                            <td><b>Action</b></td>
                        </tr>
                    </thead>
                    <tbody>
                        {movie !== null && movie.map((item,index)=>{
                        return(
                            <tr key={item.id}>
                                    <td>{index+1}</td>
                                    <td>{item.title}</td>
                                    <td>{item.description}</td>
                                    <td>{item.Year}</td>
                                    <td>{item.duration}</td>
                                    <td>{item.genre}</td>
                                    <td>{item.rating}</td>
                                    <td>
                                        <button>Edit</button><br/>
                                        <button onClick={deleteForm}>Delete</button>
                                    </td>
                            </tr>
                        )
                        })}
                    </tbody>
                </table>
                <h1>Movies Form</h1>
                <form onSubmit={submitForm}>
                    <table style={{border:'none'}} className={'tableForm-content'}>
                        <tr>
                            <th>Title:</th>
                            <td><input required type="text" value={input.title} onChange={changeFormTitle}/></td>
                        </tr>
                        <tr>
                            <th>Description:</th>
                            <td><textarea required value={input.description} onChange={changeFormDescription}/></td>
                        </tr>
                        <tr>
                            <th>Year:</th>
                            <td><input required type="number" value={input.year} onChange={changeFormYear}/></td>
                        </tr>
                        <tr>
                            <th>Duration:</th>
                            <td><input required type="number" value={input.duration} onChange={changeFormDuration}/></td>
                        </tr>
                        <tr>
                            <th>Genre:</th>
                            <td><input required type="text" value={input.genre} onChange={changeFormGenre}/></td>
                        </tr>
                        <tr>
                            <th>Rating:</th>
                            <td><input required type="number" value={input.rating} onChange={changeFormRating}/></td>
                        </tr>
                        <tr>
                            <th>Image Url:</th>
                            <td><textarea required value={input.image_url} onChange={changeFormImageURL}/></td>
                        </tr>
                    </table>
                    <button style={{marginLeft:'40px',marginTop:'50px'}}>submit</button>
                </form>
            </div>
        </>
    )
}

export default MovieForm