import React ,{useContext}from 'react'
import '../css/menustyle.css'
import {MoviesContext} from './MoviesContext'

const MovieList=()=>{
    const [movie]=useContext(MoviesContext)
    return(
        <>
            <section class="content-home">
                <h1 class="judul-home">Daftar Film Film Terbaik</h1>
                <div>
                    {movie!== null && movie.map((item)=>{
                        return(
                            <>
                                <div key={item.id}>
                                    <h3>{item.title}</h3>
                                    <div className={'review'}>
                                        <div className={'gambar'}>
                                            <img src={item.image_url} alt={''} style={{
                                                width:'700px',
                                                height:'500px',
                                                marginLeft:'-100px'
                                            }}/>
                                        </div>
                                        <div className={'review2'}>
                                            <b>Rating {item.rating}</b><br/>
                                            <b>Durasi: {item.duration/60} jam</b><br/>
                                            <b>genre: {item.genre}</b>
                                        </div>
                                    </div>
                                    <p>{item.description}</p>
                                </div>
                                <br/><hr/><br/>
                            </>
                        )
                    })}
                </div>
            </section>
        </>
    )
}

export default MovieList