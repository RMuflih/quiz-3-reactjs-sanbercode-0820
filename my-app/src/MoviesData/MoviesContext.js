import React, {useState,createContext, useEffect} from 'react'
import Axios from 'axios'

export const MoviesContext = createContext()

export const MoviesProvider = props =>{
    const [movie,setMovie]=useState(null)
    useEffect(()=>{
        if(movie===null){
            Axios.get('http://backendexample.sanbercloud.com/api/movies')
            .then(res=>{
                setMovie(res.data)
            })
        }
    },[movie])

    return(
        <MoviesContext.Provider value={[movie,setMovie]}>
            {props.children}
        </MoviesContext.Provider>
    )
}