import React from 'react'
import '../css/mainstyle.css'
import Logo from '../source/logo.png'
import {Switch,Route,BrowserRouter as Router,Link} from 'react-router-dom'
import Home from '../Home/Home'
import About from '../About/About'
import MovieListEditor from '../MovieListEditor/MovieListEditor'

const Main = () =>{
    return(
        <>
            <Router>
                <header>
                    <img src={Logo} width={"200px"} alt={''}/>
                    <nav>
                        <ul>
                            <li>
                                <Link to='/'>Home</Link>
                            </li>
                            <li>
                                <Link to='/About'>About</Link>
                            </li>
                            <li>
                                <Link to='/MovieListEditor'>Movie List Editor</Link>
                            </li>
                            <li>
                                <Link to='/Login'>Login</Link>
                            </li>
                        </ul>
                    </nav>
                </header>
                <Switch>
                    <Route exact path='/' component={Home}/>
                    <Route path='/About'>
                        <About/>
                    </Route>
                    <Route path='/MovieListEditor'>
                        <MovieListEditor/>
                    </Route>

                </Switch>    
                <footer>
                    <h5>copyright &copy; 2020 by Sanbercode</h5>
                </footer>
            </Router>
        </>
    )
}

export default Main